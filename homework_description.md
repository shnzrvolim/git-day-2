### Task #1

In this task you need to:

- Create a public repository on GitLab with name git-day-2 (check readme file while creating)
- On your computer create two separate folders: user1 and user2
- Clone the repository using `https` in both folders

### Task #2

In this task you need to:

- Move to repository in `user1` folder and change git's username to user1 using `git config`
- Add index.html file  with following content

 ```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <link rel="stylesheet" href="index.css" />
</head>
<body>
  <h1>Hello World!</h1>
</body>
</html>>
 ```

- Open index.html file in browser and see result

- Publish your changes

### Task #3

In this task you need to:

- Move to repository in `user2` folder and pull latest changes from remote repository
- Change username for git locally to `user2` via `git config`
- Add index.css file  with following content

 ```css
h1 {
	background: red
}
 ```

- Open index.html file in browser and see result (you should see red text)
- Publish your changes